<?php
return [
    'dependencies' => [
        'backend'
    ],
    'tags' => [
        'backend.form',
    ],
    'imports' => [
        '@typoworx/typo3-rte-ckeditor-fontawesome5/plugin.js' => 'EXT:tnm_ckeditor_addons/Resources/Public/JavaScript/CkEditor/Plugins/ckeditor-fontawesome5/plugin.js',
    ],
];
