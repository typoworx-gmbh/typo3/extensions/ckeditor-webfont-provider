import * as Core from '@ckeditor/ckeditor5-core';
import * as UI from '@ckeditor/ckeditor5-ui';
import { toWidget } from '@ckeditor/ckeditor5-widget';
import {default as modalObject} from "@typo3/backend/modal.js";

export default class IfFontawesome5 extends Core.Plugin {
    static get pluginName() {
        return 'IfFontawesome5';
    }

    static get requires() {
        return [];
    }

    get _defaultAttributes() {
        return {
            'class': '',
            'aria-hidden': 'true'
        };
    }

    get _modelName() {
        return 'fontGlyphTag';
    }

    init(){
        this._addFontawesomeCss();
        this._defineSchema();
        this._defineConverters();

        let button = new UI.ButtonView();

        this.editor.ui.componentFactory.add(
            IfFontawesome5.pluginName,
            () => {
                button.set({
                    label: 'Insert FontAwesome icon',
                    withText: false,
                    command: 'ckeditor_fa',
                    icon: '<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n' +
                        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\n' +
                        '<svg width="100%" height="100%" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">\n' +
                        '    <use id="Hintergrund" xlink:href="#_Image1" x="0" y="0" width="16px" height="16px"/>\n' +
                        '    <defs>\n' +
                        '        <image id="_Image1" width="16px" height="16px" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QUYCi8USdD7QQAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAGnSURBVDjLpZNNT1NBFIafM3duewtaMZCQhhBBDWpcgSHElSTGvWsWLt2buOd/sICNa9z4C4zgStEFH8q3FSstbbmtuba09x4X4E1bYmLhJDPJZHKemfd9Z2R4/qVyiTIArnGwYto2/pdqjAjP7z/i6a0JamEDAVI2QTrh0esmccQQqcYjZV2clsOsEeHF+BPWSj94s/uZB4MjTA+NMXy1n+NawFJuk0+FbwTNBteSKWbGpni9/ZG1cg5VxUaqLKy+I2jWmXv8jOmhu21XnLkzxW7liGylyMTgDa64HuvlHBvln4QoFiBC6fN6zjX/rdH0AKPpgXgdRmG7iSKg3WShHSl0XSKXA1jTkkJMOqMeVEu8/f6F/WqJHuvyMHObyczNuGHlcI/VowOiM80xoBBUmF1e5EMhS/ZXkeP6b1zjsLizwr3rGfq9Xg6DCpt+nr1qEaUNIPiNGgvrS0QojhhEhLo2+ern2fLzWGM4iUJAzktIOhZrDA0N8Ry31S2sOZUWAlYckA4/VJVXG++pNU9IGPtv4+OpEwBs+QVAYyO7SuT0XVz8R/8BxfCeROZFcMgAAAAASUVORK5CYII="/>\n' +
                        '    </defs>\n' +
                        '</svg>\n'
                }
            );

            // Insert a text into the editor after clicking the button.
            this.listenTo(button, 'execute', () => this._openElementBrowser(this.editor));

            return button;
        });
    }

    _addFontawesomeCss() {
        let head = document.getElementsByTagName('HEAD')[0],
            link = document.createElement('link')
        ;

        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = document.location.origin + '/_assets/interfrog/if_mainmetall/Fonts/fontawesome-pro/css/all.css';

        head.appendChild(link);
    }

    _openElementBrowser(e) {
        var self = this;
        let url = new URL('dialogs/font-awesome5.html?' + (new Date()).getTime(), import.meta.url).pathname;

        let bc = new BroadcastChannel('fontawesome:selected');
        let modal = modalObject.advanced({
            type: modalObject.types.iframe,
            title: 'Fontawesome',
            content: url,
            size: modalObject.sizes.medium,
            callback: (t) => {
                bc.onmessage = function (message) {
                    self._insertGlyphIcon(message.data);

                    t.hideModal();
                };
            }
        });

        modal.addEventListener('typo3-modal-hide', e => {
            bc.close();
        });
    }

    _defineSchema() {
        this.editor.model.schema.register(
            'fontGlyphTag',
            {
                allowWhere: '$text',
                isInline: true,
                //isBlock: true,
                //isObject: true,
                allowAttributes: Object.keys(this._defaultAttributes)
            }
        );

        this.editor.config.define('contentFilter', {
            attributes: {
                'class': true,
                'aria-hidden': value => /^(true|false)$/.test(value)
            },
        });
    }

    _defineConverters() {
        // Downcast conversion: Model -> View
        this.editor.conversion.for('dataDowncast').elementToElement({
            model: 'fontGlyphTag',
            view: (modelElement, { writer }) => {
                return writer.createAttributeElement('em', modelElement.getAttributes());
            }
        });

        // Upcast conversion: View -> Model
        this.editor.conversion.for('editingDowncast').elementToElement({
            view: 'em',
            model: (viewElement, { writer }) => {
                return writer.createElement('fontGlyphTag', viewElement.getAttributes());
            }
        });

        // Downcast conversion: Model -> View
        this.editor.conversion.for('downcast').elementToElement({
            model: 'fontGlyphTag',
            view: (modelElement, { writer }) => {
                return writer.createAttributeElement('em', modelElement.getAttributes());
            }
        });

        // Upcast conversion: DOM -> Model
        this.editor.conversion.for('upcast').elementToElement({
            view: (viewElement) => viewElement.is('em') && viewElement.hasClass('fa'),
            model: (viewElement, { writer }) => {
                return writer.createElement('fontGlyphTag', viewElement.getAttributes());
            }
        });
    }

    _insertGlyphIcon(iconClass = '') {
        if (typeof iconClass === 'object') {
            iconClass = iconClass.join(' ');
        }

        let fragment = this.editor.model.change(writer => {
            let fontGlyphTag =  writer.createElement(
                'fontGlyphTag',
                {
                    class: iconClass,
                    'aria-hidden': 'true'
                }
            );

            return fontGlyphTag;
        });

        this.editor.model.insertContent(fragment, this.editor.model.document.selection.getFirstPosition());
    }
}
