(function () {
    "use strict";
    "use 'esversion:6'";

    let fontSourceUri = document.location.origin + '/_assets/interfrog/if_mainmetall/Fonts/fontawesome-pro/',
        iconContainer = document.querySelector('#faIcons')
    ;

    function createFilterButton(name, fontTypePrefix) {
        let buttonContainer = document.querySelector('#font-variant-filter'),
            button = document.createElement('button')
        ;

        button.classList = 'btn btn-sm btn-dark w-15';
        button.innerText = name || 'Undefined';

        fontTypePrefix && button.setAttribute('data-glyph-prefix', fontTypePrefix);
        buttonContainer.appendChild(button)

        return button;
    }

    function initialize() {
        let fontVariantsDefinition = {},
            head = document.getElementsByTagName('HEAD')[0],
            link = document.createElement('link')
        ;

        let fontConfigurationContainer = document.getElementById('font-types')
        if (fontConfigurationContainer.innerText.length > 0) {
            fontVariantsDefinition = JSON.parse(fontConfigurationContainer.innerText)
        }
        else if (fontConfigurationContainer.src !== undefined) {
            fetch(
                fontConfigurationContainer.src,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            )
                .then(response => {
                    response.ok || console.error(response.statusText);

                    return (response.text());
                })
                .then(payload => {
                    fontConfigurationContainer.innerText = payload;

                    return initialize();
                })
                .catch(error => {
                    console.error(error || 'undefined AJAX error');
                })
            ;

            // We have to wait for onReadyStateChange-Event
            return;
        }

        // Prepare Style-Tag-Creator
        link.rel = 'stylesheet';
        link.type = 'text/css';

        iconContainer.setAttribute('data-filter', '');

        Array.from(Object.entries(fontVariantsDefinition)).forEach(function(fontDefinition) {
            let file = fontDefinition.shift(),
                config = fontDefinition.shift(),
                fontVariant = config.prefix
            ;

            window.fontTypesDefinitions = Object.assign(window.fontTypesDefinitions || {});
            window.fontTypesDefinitions[ fontVariant ] = config;

            link = link.cloneNode(true);
            link.href = fontSourceUri + 'css/fontawesome.css';
            head.appendChild(link);

            link = link.cloneNode(true);
            link.href = fontSourceUri + 'css/' + file;
            head.appendChild(link);

            let filterButton = createFilterButton(config.name, fontVariant);

            filterButton.addEventListener('click', e => {
                let glyphPrefix = e.target.dataset.glyphPrefix;

                if (glyphPrefix === '') {
                    e.stopPropagation();
                    return;
                }

                e.target.classList.toggle('active');

                let filterAttributes = new Set((iconContainer.dataset.filter.length > 0 ? iconContainer.dataset.filter.trim().split(',') : []));
                if (filterAttributes.has(glyphPrefix)) {
                    filterAttributes.delete(glyphPrefix);
                } else {
                    filterAttributes.add(glyphPrefix);
                }

                iconContainer.setAttribute('data-filter', [...filterAttributes]);

                refreshIconContainer();
            });

            if (filterButton && config['active'] === true) {
                filterButton.click();
            }
        });
    }

    function refreshIconContainer() {
        let iconListMarkup = '';

        if (!iconContainer.dataset.filter) {
            return;
        }

        iconContainer.dataset.filter.trim().split(',').forEach(variant => {
            let fontVariant = window['fontTypesDefinitions'][ variant ];

            if (variant !== undefined && window.fontTypesDefinitions[ variant ] !== undefined) {
                if (window.fontTypesDefinitions[ variant ].name !== undefined) {
                    iconListMarkup += '<div class="col col-sm-12"><strong>' + fontVariant['name'] + '</strong></div></div>';
                }
            }

            Array.from(fontVariant.icons).forEach(glyph => {
                iconListMarkup += `<a href="#" class="col col-sm-3 col-md-2 col-lg-1 p-2" onclick="return false;" data-icon="${variant} fa-${glyph}"><i class="${variant} fa-${glyph}"></i>${glyph}</a>`;
            });
        });

        iconContainer.innerHTML = iconListMarkup;
        iconContainer.querySelectorAll('a[data-icon]').forEach(function (link) {
            link.addEventListener('click', e => {
                var bc = new BroadcastChannel('fontawesome:selected');

                ///console.debug('Sending Broadcast message to use', e.target.dataset.icon);
                bc.postMessage([e.target.dataset.icon]);
            });
        });

        // Delegate to Search
        let search = document.querySelector('input[name="search"]'),
            dispatchEvent = document.createEvent('KeyboardEvent')
        ;
        dispatchEvent[ dispatchEvent.initKeyEvent ? 'initKeyEvent' : 'initKeyboardEvent' ]('keyup', true, true, window, false, false, false, false, 0, 0);
        search.dispatchEvent(dispatchEvent)
    }

    // Search Feature
    var search = document.querySelector('input[name="search"]');
    search.addEventListener('keyup', e => {
        var search = e.target.value;

        iconContainer.querySelectorAll('a[data-icon]').forEach(item => {
            let icon = item.dataset['icon'];

            if (icon && icon.indexOf(search) >= 0) {
                item.style.display = 'inline-block';
            } else {
                item.style.display = 'none';
            }
        })
    });

    initialize();
})();
